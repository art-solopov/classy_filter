module ClassyFilter
  module Coercions
    extend self

    def coerce(value, coercion)
      case coercion
      when Symbol then send(coercion, value)
      when ->(x) { x.respond_to?(:call) } then coercion.call(value)
      end
    end

    private

    # Scalar conversions
    # Those are methods that receive a value and return
    # the coerced value

    def integer(value)
      Integer(value)
    end

    def float(value)
      Float(value)
    end

    def boolean(value)
      case value
      when true, 'true', 'TRUE', 1, '1' then true
      when false, 'false', 'FALSE', 0, '0' then false
      end
    end

    def date(value)
      case value
      when Date then value
      when Array then Date.new(*value)
      else Date.parse(value.to_s)
      end
    end

    def timestamp(value)
      case value
      when Time then value
      when DateTime then value.to_time
      when Array then Time.new(*value)
      else DateTime.parse(value.to_s).to_time
      end
    end
  end
end
