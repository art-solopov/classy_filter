module ClassyFilter
  module Predicates
    extend self

    def eq_in(ds, attribute, input)
      ds.where(attribute => input)
    end

    alias eq eq_in
    alias :in eq_in

    # TODO: think about refactoring

    def gt(ds, attribute, input)
      ds.where { |_r| sequel_col(ds, attribute) > input }
    end

    def lt(ds, attribute, input)
      ds.where { |_r| sequel_col(ds, attribute) < input }
    end

    def gteq(ds, attribute, input)
      ds.where { |_r| sequel_col(ds, attribute) >= input }
    end

    def lteq(ds, attribute, input)
      ds.where { |_r| sequel_col(ds, attribute) <= input }
    end

    def starts_with(ds, attribute, input)
      ds.where { |_r| Sequel.like(sequel_col(ds, attribute), input + '%') }
    end

    def starts_with_i(ds, attribute, input)
      ds.where { |_r| Sequel.ilike(sequel_col(ds, attribute), input + '%') }
    end

    def contains(ds, attribute, input)
      ds.where { |_r| Sequel.like(sequel_col(ds, attribute), "%#{input}%") }
    end

    def contains_i(ds, attribute, input)
      ds.where { |_r| Sequel.ilike(sequel_col(ds, attribute), "%#{input}%") }
    end

    private

    def sequel_col(ds, attribute)
      Sequel[ds.first_source_table][attribute.to_sym]
    end
  end
end
