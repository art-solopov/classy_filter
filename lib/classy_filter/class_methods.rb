module ClassyFilter
  module ClassMethods
    attr_reader :filter_fields

    def filter_field(param_name, attribute: nil,
                     predicate: :eq_in, coercion: nil)
      @filter_fields ||= []
      @filter_fields << Field.new(param_name, attribute,
                                  predicate, coercion)
      define_method(param_name) { read_param(param_name) }
    end

    def predicate(name, body)
      predicates_module.define_method(name, &body)
    end

    def coercion(name, body)
      coercions_module.define_method(name, &body)
      coercions_module.send(:private, name)
    end

    modules = {
      predicates: ClassyFilter::Predicates,
      coercions: ClassyFilter::Coercions
    }.freeze

    modules.each do |name, default|
      define_method(:"#{name}_module") do |&block|
        (instance_variable_get(:"@_#{name}") || default).tap do |mod|
          mod.instance_eval(&block) if block
        end
      end
    end

    protected

    def build_predicates_module!(base)
      @predicates_module = Module.new do
        extend base.predicates_module
        extend self
      end
    end

    def build_coercions_module!(base)
      @coercions_module = Module.new do
        extend base.coercions_module
        extend self
      end
    end

    private

    def inherited(subclass)
      subclass.build_predicates_module!(self)
      subclass.build_coercions_module!(self)
    end
  end
end
