require 'forwardable'

require 'classy_filter/version'
require 'classy_filter/predicates'
require 'classy_filter/coercions'
require 'classy_filter/class_methods'

module ClassyFilter
  Field = Struct.new(:param_name, :attribute, :predicate, :coercion)

  class Base
    extend Forwardable

    extend ClassMethods

    attr_reader :params

    def initialize(params = nil)
      @params = params || {}
    end

    def call(dataset)
      filter_fields.reduce(dataset) do |ds, (value, field)|
        value = coercions_module.coerce(value, field.coercion) if field.coercion
        attribute = field.attribute || field.param_name
        predicates_module.send(field.predicate, ds, attribute, value)
      end
    end

    private

    def_delegator :"self.class", :filter_fields, :all_filter_fields
    def_delegator :"self.class", :predicates_module
    def_delegator :"self.class", :coercions_module

    def filter_fields
      all_filter_fields
        .to_a
        .lazy
        .map { |ff| [read_param(ff.param_name), ff] }
        .reject { |param, _| param.nil? }
        .reject { |param, _| param.is_a?(String) && param.empty? }
    end

    def read_param(param_name)
      params[param_name.to_s] || params[param_name.to_sym]
    end
  end
end
