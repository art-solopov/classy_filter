RSpec.describe ClassyFilter::Base do
  before do
    db.create_table(:things) do
      primary_key :id
      Integer :int_var
      String :str_var
      Time :time_var
    end

    db[:things].import(
      %i[int_var str_var time_var],
      [
        [10, 'qwe', Time.new(2018, 2, 1, 12, 0, 0)],
        [20, 'qwe', Time.new(2018, 6, 5, 14, 0, 0)],
        [10, 'xyz', Time.new(2018, 6, 6, 2, 0, 0)],
        [10, 'abc', Time.new(2018, 6, 5, 10, 0, 0)],
        [15, 'Abc', Time.new(2018, 6, 5, 20, 0, 0)]
      ]
    )
  end

  let(:filtered_ds) { klass.new(params).(db[:things]) }
  subject { filtered_ds.all }

  describe 'simple predicate' do
    let!(:klass) do
      Class.new(described_class) do
        filter_field :int_var
        filter_field :str_var
      end
    end
    let(:params) { { int_var: 10, str_var: %w[qwe abc] } }

    it { expect(klass.new(params).int_var).to eq(10) }
    it { expect(klass.new(params).str_var).to eq(%w[qwe abc]) }
    it { expect(subject.length).to eq(2) }

    it do
      expect(subject.map { |e| e[:int_var] }).to eq([10, 10])
    end

    it do
      expect(subject.map { |e| e[:str_var] }).to eq(%w[qwe abc])
    end

    context 'with nil input' do
      let(:params) { nil }
      it { expect(klass.new(params).int_var).to be_nil }
      it { expect(klass.new(params).str_var).to be_nil }
      it { expect { subject }.not_to raise_error }
      it { expect(subject).to match_array db[:things].all }
    end
  end

  describe 'predicates' do
    describe 'advanced' do
      let!(:klass) do
        Class.new(described_class) do
          filter_field :int_var_min, attribute: :int_var, predicate: :gteq
          filter_field :str_prefix, attribute: :str_var, predicate: :starts_with_i
          filter_field :str_cont, attribute: :str_var, predicate: :contains_i
        end
      end

      let(:params) { { int_var_min: 15, str_prefix: 'ab', str_cont: 'bc' } }

      it { expect(subject.length).to eq(1) }

      it do
        expect(subject.map { |e| e[:int_var] }).to eq([15])
      end

      it do
        expect(subject.map { |e| e[:str_var] }).to eq(%w[Abc])
      end
    end

    describe 'custom' do
      let!(:klass) do
        Class.new(described_class) do
          predicate :smaller_than_10x, ->(ds, attr, inp) { ds.where { |_r| Sequel[attr] <= 10 * Integer(inp) } }

          predicates_module do
            def bigger_than_half(dataset, attribute, input)
              dataset.where { |_r| Sequel[attribute] >= 0.5 * Integer(input) }
            end
          end

          filter_field :int_10x, attribute: :int_var, predicate: :smaller_than_10x
          filter_field :int_05x, attribute: :int_var, predicate: :bigger_than_half
          filter_field :str_var
        end
      end

      let(:params) { { int_10x: 1, int_05x: 2 } }

      it { expect(klass.predicates_module.methods).to include(:smaller_than_10x) }
      it { expect(klass.predicates_module.methods).to include(:bigger_than_half) }
      it { expect(subject.length).to eq(3) }
    end
  end

  describe 'coercions' do
    let!(:klass) do
      Class.new(described_class) do
        filter_field :int_var, coercion: :integer
        filter_field :str_var
      end
    end

    context 'basic' do
      let(:params) { { 'int_var' => '10' } }

      it { expect(subject.length).to eq(3) }
      it { expect(filtered_ds.sql).not_to include("'10'") }
    end

    context 'custom method' do
      before do
        klass.coercions_module do
          def lower_str(value)
            value.to_s.downcase
          end
        end

        klass.filter_field :lower_str_var, attribute: :str_var, coercion: :lower_str
      end

      let(:params) { { 'lower_str_var' => 'QWE' } }

      it { expect(subject.length).to eq(2) }
      it { expect(filtered_ds.sql).to include("'qwe'") }
    end

    context 'custom block' do
      before do
        klass.coercion :lower_str, ->(val) { val.to_s.downcase }
        klass.filter_field :lower_str_var, attribute: :str_var, coercion: :lower_str
      end

      let(:params) { { 'lower_str_var' => 'QWE' } }

      it { expect(subject.length).to eq(2) }
      it { expect(filtered_ds.sql).to include("'qwe'") }
    end
  end
end
