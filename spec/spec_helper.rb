require "bundler/setup"
require 'logger'

require "classy_filter"

require 'sequel'

module SpecHelpers
  private

  def db
    @db ||= ::Sequel.sqlite(
      logger: ENV['DB_LOG'] ? Logger.new(STDOUT) : nil
    )
  end
end

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = ".rspec_status"

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end

  if config.files_to_run.one?
    config.default_formatter = 'documentation'
  else
    config.default_formatter = 'progress'
  end

  config.include(SpecHelpers)
end
