
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "classy_filter/version"

Gem::Specification.new do |spec|
  spec.name          = "classy_filter"
  spec.version       = ClassyFilter::VERSION
  spec.authors       = ["Artemiy Solopov"]
  spec.email         = ["art-solopov@yandex.ru"]

  spec.summary       = %q{A cutomizeable class-based filtering/searching library for Sequel}
  spec.description   = spec.summary
  spec.homepage      = "https://gitlab.com/art-solopov/classy_filter"
  spec.license = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.require_paths = ["lib"]

  spec.add_runtime_dependency 'sequel', '~> 5.0'

  spec.add_development_dependency "bundler", "~> 1.16"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
  spec.add_development_dependency 'sqlite3', '~> 1.0'
  spec.add_development_dependency 'pry'
end
